from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from online_lib.library_docs.api.views import (
    CategoryViewSet,
    ChapterViewSet,
    DocumentViewSet,
    RequestViewSet,
    DocumentQueueViewSet,
)
from online_lib.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("categories", CategoryViewSet, basename="category")
router.register("documents", DocumentViewSet, basename="document")
router.register("chapters", ChapterViewSet, basename="chapter")
router.register("requests-docs", RequestViewSet, basename="request-docs")
router.register("new-docs", DocumentQueueViewSet, basename="new-docs")


app_name = "api"
urlpatterns = router.urls
