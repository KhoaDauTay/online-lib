from django.db import models
from django.core.mail import send_mail
from django.shortcuts import reverse
from django.conf import settings
from django.template.defaultfilters import slugify
from datetime import timedelta
# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(null=False, unique=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    class Meta:
        db_table = 'authors'
        managed = True
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'


class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(null=False, unique=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    class Meta:
        db_table = 'categories'
        managed = True
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Document(models.Model):
    category = models.ManyToManyField(Category, related_name='documents')
    author = models.ForeignKey(Author, null=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    image = models.ImageField()
    description = models.TextField()
    votes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    slug = models.SlugField(null=False, unique=True)
    is_visible = models.BooleanField(default=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    class Meta:
        db_table = 'documents'
        managed = True
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'


class Chapter(models.Model):
    document = models.ForeignKey(
        Document,
        related_name='chapter_docs',
        null=False,
        on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    content = models.TextField()
    is_free = models.BooleanField(default=False)
    slug = models.SlugField(null=False, unique=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    class Meta:
        db_table = 'chapters'
        managed = True
        verbose_name = 'Chapter'
        verbose_name_plural = 'Chapters'


class Request(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=False,
        on_delete=models.CASCADE)
    document = models.ForeignKey(
        Document,
        null=False,
        on_delete=models.CASCADE)
    PENDING = 'PENDING'
    APPROVED = 'APPROVED'
    REJECTED = 'REJECTED'
    CANCELLED = 'CANCELLED'
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (APPROVED, 'Approved'),
        (REJECTED, 'Rejected'),
        (CANCELLED, 'Cancelled'),
    )
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default=PENDING,
    )
    is_permission = models.BooleanField(default=False, null=False)
    limit_time = models.DateTimeField(blank=True, null=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Request from {self.user.username} to {self.document.name}"

    def save(self, *args, **kwargs):
        if self.status == 'APPROVED':
            self.is_permission = True
            if self.update_at is not None:
                self.limit_time = self.update_at + timedelta(days=7)
                return super().save(*args, **kwargs)
        elif self.status == 'REJECTED':
            self.limit_time = None
            self.is_permission = False
            return super().save(*args, **kwargs)
        elif self.status == 'CANCELLED':
            self.delete()
        else:
            return super().save(*args, **kwargs)

    class Meta:
        db_table = 'requests'
        managed = True
        verbose_name = 'Request'
        verbose_name_plural = 'Requests'


class DocumentQueue(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    description = models.TextField(
        default="Get the link new book if system don't have",
        blank=True)
