from django.contrib import admin

from online_lib.library_docs.models import Category, Author, Document, Chapter, Request, DocumentQueue


class AuthorAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "create_at", "update_at")
    list_filter = ("name", "create_at", "update_at")
    search_fields = ("name","create_at")
    prepopulated_fields = {'slug': ('name',)}

class CatergoryAdmin(admin.ModelAdmin):
    list_display = ("name", "slug")
    list_filter = ("name", "create_at", "update_at")
    search_fields = ("name","create_at")
    prepopulated_fields = {'slug': ('name',)}

class DocumentAdmin(admin.ModelAdmin):
    list_display = ("name", "author", "description",
                    "slug", "is_visible", "create_at", "update_at")
    list_filter = ("name", "author", "category",
        "views", "votes", "is_visible", "create_at", "update_at")
    search_fields = ("name", "author" )
    prepopulated_fields = {'slug': ('name',)}

class ChapterAdmin(admin.ModelAdmin):
    list_display = ("title", "slug")
    list_filter = ("title", "create_at", "update_at")
    search_fields = ("title", "create_at")
    prepopulated_fields = {'slug': ('title',)}


class RequestAdmin(admin.ModelAdmin):
    list_display = ("user", "document", "status",
        "is_permission", "limit_time", "create_at", "update_at")

    list_filter = ("user", "document", "status",
        "is_permission", "limit_time", "create_at", "update_at")

    search_fields = ("user", "document", "status")

admin.site.register(Category, CatergoryAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Chapter, ChapterAdmin)
admin.site.register(Request, RequestAdmin)
admin.site.register(DocumentQueue)
