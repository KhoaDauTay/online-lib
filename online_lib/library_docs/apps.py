from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class LibraryDocsConfig(AppConfig):
    name = 'online_lib.library_docs'


