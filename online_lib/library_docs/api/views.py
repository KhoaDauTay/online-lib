from rest_framework import viewsets, permissions, filters, status
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .serializers import (
    ChapterSerializer,
    DocumentSerializer,
    CategorySerializer,
    RequestSerializer,
    DocumentQueueSerializer,
)
from online_lib.library_docs.models import (
    Category,
    Document,
    Chapter,
    Request,
    DocumentQueue,
)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticated,)


class DocumentViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = 'chapter_docs'
    filter_fields = (
        'category', 'name',
        'author', 'votes', 'views',
    )
    ordering = ('id',)
    search_fields = ('name', 'author' )

    def list(self, request):
        queryset = Document.objects.filter(is_visible=True)
        serializer = self.serializer_class(
            queryset,
            context={'request': request},
            many=True
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=id):
        docs = get_object_or_404(self.queryset, pk=pk)
        if docs.is_visible:
            serializer = self.serializer_class(docs)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

    # def create(self, request, *args, **kwargs):
    #     docs = get_object_or_404(self.queryset, pk=pk)
    #     check = Request.objects.get(user=request.user, document=docs)
    #     time_now = timezone.now()
    #     if check.limit_time > time_now:
    #         if check.is_permission:
    #             docs.votes += int(request.data.get("votes"))
    #             docs.save()
    #             serializer = serializer_class(queryset)
    #             return Response(
    #                 serializer.data,
    #                 status=status.HTTP_201_CREATED)
    #         else:
    #             return Response(status=status.HTTP_404_NOT_FOUND)
    #     else:
    #         return Response(status=status.HTTP_404_NOT_FOUND)


class ChapterViewSet(viewsets.ViewSet):
    queryset = Chapter.objects.all()
    serializer_class = ChapterSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        serializer = self.serializer_class(self.queryset, context={"user":request.user}, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=id):
        chapter = get_object_or_404(self.queryset, pk=pk)
        if chapter.is_free:
            serializer = self.serializer_class(chapter, context={"user":request.user})
            return Response(serializer.data)
        else:
            try:
                check = Request.objects.get(
                    user=request.user,
                    document=chapter.document
                )
                time_now = timezone.now()
                if check.limit_time > time_now:
                    if check.is_permission:
                        serializer = self.serializer_class(chapter, context={"user":request.user})
                        return Response(
                            serializer.data,
                            status=status.HTTP_200_OK
                        )
                    else:
                        return Response(status=status.HTTP_404_NOT_FOUND)
                else:
                    return Response(status=status.HTTP_404_NOT_FOUND)
            except:
                return Response(status=status.HTTP_404_NOT_FOUND)


class RequestViewSet(viewsets.ModelViewSet):
    queryset = Request.objects.all()
    serializer_class = RequestSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        request_doc_id = int(request.data.get("document_id"))
        document = Document.objects.get(id=request_doc_id)
        request_qs = Request.objects.filter(
            user=request.user,
            document=document)
        if request_qs.exists():
            return Response(
                {"Fail": f"The request {request_qs[0].get_status_display()} is exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        else:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)


class DocumentQueueViewSet(viewsets.ModelViewSet):
    queryset = DocumentQueue.objects.all()
    serializer_class = DocumentQueueSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        check_docs = Document.objects.filter(name=request.data.get("name"))
        check_category = Category.objects.filter(
            name=request.data.get("category"))
        print(check_category.name)
        if check_docs.count() > 0 and check_category.count() > 0:
            serializer = DocumentSerializer(check_docs)
            return Response(serializer.data,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
