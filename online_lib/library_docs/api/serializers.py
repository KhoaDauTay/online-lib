from rest_framework import serializers
from rest_framework.reverse import reverse
from online_lib.library_docs.models import (
    Category,
    Author,
    Document,
    Chapter,
    Request,
    DocumentQueue,
)
from rest_framework.decorators import action

class RequestSerializer(serializers.ModelSerializer):
    document_id = serializers.IntegerField()

    class Meta:
        model = Request
        fields = ('status', 'document_id', 'limit_time')


class CategorySerializer(serializers.ModelSerializer):
    documents = serializers.StringRelatedField(many=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'documents')


class DocumentSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=True)
    chapter_docs = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api:chapter-detail'
    )

    class Meta:
        model = Document
        fields = (
            'id', 'category',
            'name', 'author',
            'chapter_docs', 'votes',
            'views', 'image',
            'is_visible',
        )


class ChapterSerializer(serializers.ModelSerializer):
    time =  serializers.SerializerMethodField()
    class Meta:
        model = Chapter
        fields = ('id', 'title', 'content', 'time')

    @action(detail=True, methods=['get'])
    def get_time(self,  obj):
        check = Request.objects.get(user=self.context.get('user'), document=obj.document)
        return check.limit_time


class DocumentQueueSerializer(serializers.ModelSerializer):

    class Meta:
        model = DocumentQueue
        fields = ('id', 'name', 'category', 'description')
