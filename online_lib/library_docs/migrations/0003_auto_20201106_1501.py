# Generated by Django 3.0.11 on 2020-11-06 08:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library_docs', '0002_auto_20201106_1501'),
    ]

    operations = [
        migrations.RenameField(
            model_name='document',
            old_name='vote',
            new_name='votes',
        ),
    ]
