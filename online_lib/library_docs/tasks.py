from django.conf import settings
from config import celery_app
from django.core.mail import send_mail


@celery_app.task()
def send_notification(user):
    send_mail(
        # subject
        "KhoaLib-Noticaiton",
        # message
        "Request to see your book chapter approved",
        settings.EMAIL_HOST_USER,
        [user.email]
        # from_email
    )
    return None
